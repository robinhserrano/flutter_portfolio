import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ShortIntroPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Color.fromRGBO(58, 50, 80, 1),
            body: Stack(alignment: Alignment.bottomCenter, children: <Widget>[
              Column(
                children: <Widget>[
                  Image.asset("assets/JXA0.gif", fit: BoxFit.contain)
                ],
              ),
              Container(
                  margin: EdgeInsets.only(bottom: 150),
                  width: MediaQuery.of(context).size.width - 50,
                  child: AnimatedTextKit(animatedTexts: [
                    TypewriterAnimatedText(
                        "I'm currently a 4th year Computer Science student that enjoys learning exciting stuffs and overcoming new challenges.",
                        textStyle: GoogleFonts.bebasNeue(
                            fontSize: 30,
                            color: Colors.white.withOpacity(0.92)),
                        textAlign: TextAlign.center)
                  ], isRepeatingAnimation: false))
            ])));
  }
}
