import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class SkillsPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return SafeArea(
        child: Container(
            color: Color.fromRGBO(114,137,218,1),
            alignment: Alignment.center,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
                children: <Widget>[
                    SizedBox(height: 20,),
                    Text("SKILLS",
                        style: GoogleFonts.bebasNeue(
                            fontSize: 50, 
                            color: Colors.grey[100]
                        ),
                    ),
                    SizedBox(height: 20,),
                    Skill(
                        size: 170,
                        score: 86,
                    ),
                    Wrap(
                        children: <Widget>[
                            Skill(
                                score: 81,
                                skills: "Python",
                            ),
                            Skill(
                                score: 80,
                                skills: "Javascript",
                            ),
                            Skill(
                                score: 78,
                                skills: "Java",
                            ),
                            Skill(
                                score: 75,
                                skills: "MySQL",
                            )
                        ]
                    )]
                ))
            );
        }
    }


// ignore: must_be_immutable
class Skill extends StatefulWidget {
    Color baseColor;
    String skills;
    double width;
    double handleWidth;
    double size;
    double score;
    Skill(
        {this.skills = "Flutter",
        this.baseColor = const Color(0xff303187),
        this.width = 16,
        this.handleWidth = 6,
        this.size = 150,
        this.score = 50});
    @override
    _SkillState createState() => _SkillState();
}

class _SkillState extends State<Skill> {
    @override
    Widget build(BuildContext context) {
        return Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: SleekCircularSlider(
            initialValue: widget.score,
            innerWidget: inner,
            appearance: CircularSliderAppearance(
                size: widget.size,
                customColors: CustomSliderColors(
                    trackColor: widget.baseColor
                ),
            angleRange: 245,
            startAngle: 150,
            customWidths: CustomSliderWidths(
                progressBarWidth: widget.width,
                handlerSize: widget.handleWidth,
                shadowWidth: 35)
                )
            )
        );
    }

    Widget inner(double i) {
        return Container(
            alignment: Alignment.center,
            child: Text(
                "${widget.skills}\n${i.toInt()}",
                textAlign: TextAlign.center,
                style: GoogleFonts.bebasNeue(fontSize: 24, color: Colors.grey[100]),
                ),
            );
        }
    }