import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Container(
        child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
                Observer(builder: (_) {
                    return Container(
                        child: Image.asset(
                            "assets/planets.gif",
                            fit: BoxFit.cover,
                        ),
                    );
                }
            ),
            Container(
                width: 100,
                margin: EdgeInsets.only(top: 100),
                alignment: Alignment.topCenter,
                child:  Column(
                    children: [
                        CircleAvatar(
                            backgroundImage: AssetImage('assets/dp.png'),
                            radius: 80.0
                        )
                    ],
                ),
            ),
            Container(
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.only(bottom: 100),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                        Text("Robin Andrei H. Serrano\nrobinhserrano@gmail.com\nFFUF Developer Trainee",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.nerkoOne(
                            color: Colors.white.withOpacity(0.92),
                            fontSize: 36)
                        ),
                        SizedBox(height: 30,),
                        ElevatedButton.icon(
                            icon: Icon(
                                FontAwesomeIcons.linkedin,
                                color: Colors.white,
                                size: 24.0,
                            ),
                        label: Text('LinkedIn'),
                        onPressed: () {
                            _launchURL("https://www.linkedin.com/in/robin-andrei-serrano-5b2864166/");
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Colors.blue[800],
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(20.0))
                            ),
                        ),
                        ElevatedButton.icon(
                            icon: Icon(
                                Icons.mail,
                                color: Colors.white,
                                size: 24.0,
                            ),
                        label: Text('Email'),
                        onPressed: () {
                            _launchURL("https://www.linkedin.com/in/robin-andrei-serrano-5b2864166/");
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Colors.blue[800],
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(20.0),
                                )
                            )
                        )
                    ])
                )]
            ));
        }
    }

_launchURL(String url) async {
    if (await canLaunch(url)) {
        await launch(url);
    } else {
        throw 'Could not launch $url';
    }
}